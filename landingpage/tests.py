from django.test import TestCase, Client
from .models import TDDModel
from .forms import CreateForm

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.common.keys import Keys


# Create your tests here.
class LandingPageTest(TestCase):
    def test_landing_page_is_exist(self):
        response = Client().get("")
        self.assertEquals(response.status_code, 200)

    def test_landingpage_html_file_is_used(self):
        response = Client().get("")
        self.assertTemplateUsed(response, 'landingpage.html')

    def test_Create_Model_is_exist(self):
        new_status = TDDModel.objects.create(status='test input status')
        self.assertEqual(TDDModel.objects.all().count(), 1)

    def test_valid_Create_Form_created_and_saved(self):
        data_form = {
            'status': 'semangat ngedeadline!!'
        }
        form = CreateForm(data=data_form)
        self.assertTrue(form.is_valid())
        form.save()
        self.assertEqual(TDDModel.objects.all().count(), 1)
        self.assertEqual(TDDModel.objects.get(id=1).status, 'semangat ngedeadline!!')

    def test_input_posted_is_saved_and_displayed(self):
        data = {'status': 'coba method status post'}
        response = Client().post('', data)
        content = response.content.decode('utf8')
        self.assertIn('coba method status post', content)
        self.assertEqual(
            TDDModel.objects.get(id=1).status, 'coba method status post'
        )

class FunctionalTestLandingPage(StaticLiveServerTestCase):

    def setUp(self):
        chrome_op = Options()
        chrome_op.add_argument('--no-sandbox'),
        chrome_op.add_argument('--headless'),
        chrome_op.add_argument('disable-dev-shm-usage'),
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_op)

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_post(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        new_status = selenium.find_element_by_name("status")
        new_status.send_keys("Coba Coba")
        submit = selenium.find_element_by_name('submit')
        submit.click()
        self.assertIn('Coba Coba', selenium.page_source)

