#from django.contrib import admin
from django.urls import path
from django.conf import settings
from . import views

app_name = 'landingpage'

urlpatterns = [
    path('', views.create, name='landingpage')
]