from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class TDDModel(models.Model):
    status = models.CharField(max_length=300)
    date_added = models.DateTimeField(auto_now_add=True, null=True)