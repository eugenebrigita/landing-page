from django import forms
from django.db import models
from .models import TDDModel

class CreateForm(forms.ModelForm):
    status = forms.CharField(widget=forms.Textarea(
        attrs= {
            'class': 'form-control',
            'placeholder': 'fill with your status :D',
            'name': 'status',
        }
    ), label='')

    class Meta:
        model = TDDModel
        fields = [
            'status'
        ]