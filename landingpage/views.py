from django.shortcuts import render
from .models import TDDModel
from .forms import CreateForm

# Create your views here.
def create(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            form.save()
    form = CreateForm()
    all_status = TDDModel.objects.all()
    arguments = {
        'form': form,
        'all_status': all_status,
    }
    return render(request, 'landingpage.html', arguments)