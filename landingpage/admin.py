from django.contrib import admin
from .models import TDDModel

# Register your models here.
admin.site.register(TDDModel)
