"""story URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
import landingpage.views
import landingpage_story7.views

admin.autodiscover()

app_name = 'landingpage', 'landingpage_story7'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', landingpage.views.create, name="landingpage"),
    path('story7', landingpage_story7.views.story7, name="landingpage_story7"),
]
