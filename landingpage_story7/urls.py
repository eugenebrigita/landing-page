#from django.contrib import admin
from django.urls import path
from django.conf import settings
from landingpage_story7 import views

app_name = 'landingpage_story7'

urlpatterns = [
    path('story7/', views.story7(), name='landingpage_story7')
]