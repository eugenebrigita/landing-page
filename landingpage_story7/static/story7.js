var isDarkMode = true;
var acc = $('.accordion');
    $('.circle').on("click", function() {
        var body = $('body');
        var root = document.documentElement;
        if (isDarkMode) {
            $(body).removeClass("light").addClass("dark");
            $(body).prop("--bg", "#100f2c");
            isDarkMode = false;
        } else {
            $(body).removeClass("dark").addClass("light");
            $(body).prop("--bg", "#f5f5f5");
            isDarkMode = true;
        }
    });

    acc.on('click',function(){
        $(this).toggleClass("active")
        var panel = $(this).next()
        if ($(panel).css("max-height") == "0px") {
            var newHeight = $(panel).prop('scrollHeight') + "px";
            $(panel).css({"max-height": newHeight})
        } else {
            $(panel).css({'max-height': '0px'})
        }
    });


