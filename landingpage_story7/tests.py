from django.test import TestCase, Client

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.test import LiveServerTestCase
import time

# Create your tests here.
class LandingPageTest(TestCase):
    def test_story_7_page_is_exist(self):
        response = Client().get("/story7")
        self.assertEquals(response.status_code, 200)

    def test_story_7_html_file_is_used(self):
        response = Client().get("/story7")
        self.assertTemplateUsed(response, 'story7.html')

    def test_dark_mode_toggle_is_exist(self):
        response = Client().get("/story7")
        content = response.content.decode('utf8')
        self.assertIn("🌓", content)

    def test_accordion_is_exist(self):
        response = Client().get("/story7")
        content = response.content.decode('utf8')
        self.assertIn('class="accordion', content)
        self.assertIn('class="panel', content)

# class FunctionalTestStory7(LiveServerTestCase):
#     def setUp(self):
#         chrome_op = Options()
#         chrome_op.add_argument('--no-sandbox'),
#         chrome_op.add_argument('--headless'),
#         chrome_op.add_argument('disable-dev-shm-usage'),
#         self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_op)
#
#     def tearDown(self):
#         self.selenium.quit()
#         super().tearDown()
#
#     def test_dark_mode(self):
#         selenium = self.selenium
#         selenium.get(self.live_server_url)
#         button = selenium.find_element_by_class_name('circle')
#         self.assertIn('class="light', selenium.page_source)
#         time.sleep(3)
#         button.click()
#         time.sleep(3)
#         self.assertIn('class="dark', selenium.page_source)